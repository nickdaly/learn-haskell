{-# LANGUAGE ScopedTypeVariables #-}
import Data.List

data Section = Section { segmentA :: Int, segmentB :: Int, segmentC :: Int }
type RoadSystem = [Section]

heathrowToLondon :: RoadSystem
heathrowToLondon = [Section 50 10 30, Section 5 90 20, Section 40 2 25, Section 10 8 1]

data Label = A | B | C deriving (Show, Eq)
type Path = [(Label, Int)]


roadStep :: (Path, Path) -> Section -> (Path, Path)
roadStep (pathA, pathB) (Section a b c) =
  let priceA = sum $ map snd pathA
      priceB = sum $ map snd pathB
      forwardToA = priceA + a
      crossToA = priceB + b + c
      forwardToB = priceB + b
      crossToB = priceA + a + c
      nextToA = if forwardToA <= crossToA
                then (A,a):pathA
                else (C,c):(B,b):pathB
      nextToB = if forwardToB <= crossToB
                then (B,b):pathB
                else (C,c):(A,a):pathA
  in (nextToA, nextToB)


bestHtoLPath :: Path
bestHtoLPath = [(B,10), (C,30), (A,5), (C,20), (B,2), (B,8), (C,1)]

optimalPath :: RoadSystem -> Path


optimalPath roadSystem =
  let (bestA, bestB) = foldl roadStep ([], []) roadSystem
  in if sum (map snd bestA) <= sum (map snd bestB)
     then reverse bestA
     else reverse bestB


groupsOf :: Int -> [a] -> [[a]]
groupsOf 0 _ = undefined
groupsOf _ [] = []
groupsOf n xs = take n xs : groupsOf n (drop n xs)

main = do
  contents <- getContents
  let len = length $ lines contents
      threes :: [[Int]] = groupsOf 3 (map read $
                                     -- nothing like a little input sanitization.
                                      take (len - mod len 3) $
                                      lines contents)
      roadSystem = map (\ [a,b,c] -> Section a b c) threes
      path = optimalPath roadSystem
      pathString = concat $ map (show . fst) path
      pathPrice = sum $ map snd path
  putStrLn $ "The best path is:  " ++ pathString
  putStrLn $ "The best price is: " ++ show pathPrice
